﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;



namespace Catch_a_Critter
{
    class Button
    {
        //--------------------------
        // Data
        //--------------------------
        Texture2D image = null;
        Vector2 position = Vector2.Zero;
        SoundEffect click = null;
        bool visible = true;


        //----------------------
        // Delegates
        //----------------------

            //delegate definition states what type of functions are allowed for this delegate
       public delegate void OnClick();
       public OnClick ourButtonCallback;

        public void LoadContent(ContentManager content)
        {
            image = content.Load<Texture2D>("graphics/button");
            click = content.Load<SoundEffect>("audio/buttonclick");
        }


        public void Draw(SpriteBatch spriteBatch)
        {
            if (visible == true)
                spriteBatch.Draw(image, position, Color.White);
            

        }






        public void Input()
        {
            MouseState currentState = Mouse.GetState();

            Rectangle bounds = new Rectangle(
                (int)position.X,
                (int)position.Y,
                image.Width,
                image.Height);

            //check if we have the left button held down over the button, and if its visible
            if (currentState.LeftButton == ButtonState.Pressed &&
                bounds.Contains(currentState.X, currentState.Y)
                && visible == true)
            {

                //we clicked the button

                click.Play();

                //hide the button
                visible = false;

                //Start Game
                if(ourButtonCallback != null)
                ourButtonCallback();
                
            }

        }
    




        public void Show()
        {
            visible = true;
        }









    }
}
