﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace Catch_a_Critter
{
    class Critter
    {
        //---------------------------
        // Type Definitons
        //---------------------------
       public enum Species
            //A special type of integer with only specyfic values allowed, that have names
        {
            GORILLA,  //0
            HORSE,    //1
            ELEPHANT, //2

            // -------------

            NUM       //3
        }
          
       

        //---------------------------
        // Data
        //---------------------------
        Texture2D image = null;
        Vector2 position = Vector2.Zero;
        bool alive = false;
        Score scoreObject = null;
        SoundEffect click;
        int critterValue;
        Species ourSpecies = Species.GORILLA; // 0 = gorilla, 1 = horse, 2 = elephant


        // --------------------------
        // Behaviour
        // --------------------------

        public Critter(Score newScore, Species newSpecies)
        {
            //constructor - called when the object is created
            //no return type(special)
            //helps with setting things up
            //can have arguments
            //this one allows us to access to thew game's score
            scoreObject = newScore;
            ourSpecies = newSpecies;
        }

        // --------------------------

        public void LoadContent(ContentManager content)
        {
            

            switch (ourSpecies)
            {
                case Species.GORILLA:
                    image = content.Load<Texture2D>("graphics/gorilla");
                    critterValue = 10;
                    break;

                case Species.HORSE:
                    image = content.Load<Texture2D>("graphics/horse");
                    critterValue = 30;
                    break;

                case Species.ELEPHANT:
                    image = content.Load<Texture2D>("graphics/elephant");
                    critterValue = 50;
                    break;

                default:
                    //this should never happen
                    break;
            }

            click = content.Load<SoundEffect>("audio/buttonclick");
        }
        // --------------------------
        public void Draw(SpriteBatch spriteBatch)
        {
            if (alive == true)
            {
                spriteBatch.Draw(image, position, Color.White);
            }
            
        }
        // --------------------------
        public void Spawn(GameWindow window)
        {

            alive = true;

            //determine bounds for random critter location
            int positionYMin = 0;
            int positionXMin = 0;
            int positionYMax = window.ClientBounds.Height - image.Height;
            int positionXMax = window.ClientBounds.Width - image.Width;

            //generate random location for X and Y coords

            Random rand = new Random();
            position.X = rand.Next(positionXMin, positionXMax);
            position.Y = rand.Next(positionYMin, positionYMax);
        }
        // --------------------------
        public void Despawn()
        {
            // Set critter to not alive in order to make it uclickable and invisible
            alive = false;
        }

        public void Input()
        {
            MouseState currentState = Mouse.GetState();

            Rectangle critterBounds = new Rectangle(
                (int)position.X,
                (int)position.Y,
                image.Width,
                image.Height);

            //check if we have the left button held down over the critter
            if (currentState.LeftButton == ButtonState.Pressed && 
                critterBounds.Contains(currentState.X, currentState.Y) 
                && alive == true)
            {

                //we clicked the critter

                //despawn the critter

                Despawn();
                click.Play();
                //add to the score (TODO)
                scoreObject.AddScore(critterValue);
            }

        }
    }
}
