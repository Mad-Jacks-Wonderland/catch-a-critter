﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace Catch_a_Critter
{
    class Timer
    {

        //------------------------
        // Data
        //------------------------
        float timeRemaining = 0;
        Vector2 position = new Vector2(700, 10);
        SpriteFont font = null;
        bool running = false;


        //delegate definition ststes what type of functions are allowed for this delegate
        public delegate void TimeUp();
        public TimeUp ourTimerCallback;


        //------------------------
        // Behaviour
        //------------------------



        public void LoadContent(ContentManager content)
        {
            font = content.Load<SpriteFont>("fonts/mainFont");
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            // Draw the timer to the screen using the font variable
            int timeInt = (int)timeRemaining;
            spriteBatch.DrawString(font, "Time: " + timeInt.ToString(), position, Color.White);
        }

        public void Update(GameTime gameTime)
        {
            if (running == true)
            {
                timeRemaining -= (float)gameTime.ElapsedGameTime.TotalSeconds;

                //if time runs out..
                if (timeRemaining <= 0)
                {
                    //Stop...
                    running = false;
                    timeRemaining = 0;

                    if (ourTimerCallback != null)
                        ourTimerCallback();
                }
            }
            
        }

        public void StartTimer()
        {
            running = true;
        }

        public void SetTimer(float newTime)
        {
            timeRemaining = newTime;
        }
    }
}
