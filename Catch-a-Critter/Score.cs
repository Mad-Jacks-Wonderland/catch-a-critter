﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace Catch_a_Critter
{
    class Score
    {

        //------------------------
        // Data
        //------------------------
        int value = 0;
        Vector2 position = new Vector2(10, 10);
        SpriteFont font = null;


        //------------------------
        // Behaviour
        //------------------------

        public void AddScore(int toAdd)
        {
            //adds provided number to our current score
            value += toAdd;
        }

        public void LoadContent(ContentManager content)
        {
            font = content.Load<SpriteFont>("fonts/mainFont");
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            // Draw the score to the screen using the font variable
            spriteBatch.DrawString(font,"Score: " + value.ToString() , position, Color.White);
        }

        public void resetScore()
        {
            value = 0;
        }
           
    }
}
