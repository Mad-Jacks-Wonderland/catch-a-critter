﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using System;

namespace Catch_a_Critter
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        const int MAX_CRITTERS = 20;
        Critter[] critterPool = new Critter[MAX_CRITTERS]; // critter array - array of critter references
        Score ourScore; // a blank adress for player's score
        const float SPAWN_DELAY = 3f; //time before critter spawns
        float timeToNextSpawn = SPAWN_DELAY;
        int currentCritterIndex = 0;
        Button startButton = null;
        bool playing = false;
        Timer gameTimer = null;
        const float GAME_LENGTH = 30f;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here

            //Create and load our button
            startButton = new Button();
            startButton.LoadContent(Content);
            // set the function that will be called when ther button is clicked
            //(+= means it adds to any existing functions set there)
            startButton.ourButtonCallback += StartGame;


            //create score and load its contents
            ourScore = new Score();
            ourScore.LoadContent(Content);

            //Create timer object and load its contents

            gameTimer = new Timer();
            gameTimer.LoadContent(Content);
            gameTimer.SetTimer(GAME_LENGTH);
            gameTimer.ourTimerCallback += EndGame;

            //initialize RNG for critter species
            Random rand = new Random();


            //call the critters function to load its content
            
            for(int i = 0; i < MAX_CRITTERS; ++i)
            {

                Critter.Species newSpecies = (Critter.Species)rand.Next(0, (int)Critter.Species.NUM);

                //creating a new critter
                Critter newCritter = new Critter(ourScore, newSpecies);

                //Loading content to the new critter
                newCritter.LoadContent(Content);

                //Add newly created critter to our pool
                critterPool[i] = newCritter;
            }

           

            IsMouseVisible = true;

           // ourCritter.image = Content.Load<Texture2D>("graphics/gorilla");
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here

            

            if (playing == true)
            {

                gameTimer.Update(gameTime);

                //Should critters Spawn?
                timeToNextSpawn -= (float)gameTime.ElapsedGameTime.TotalSeconds;
                if (timeToNextSpawn <= 0f)
                {
                    //reset spawn timer
                    timeToNextSpawn = SPAWN_DELAY;
                    //spawn new critter


                    //spawn next critter on list
                    critterPool[currentCritterIndex].Spawn(Window);
                    ++currentCritterIndex;
                    if (currentCritterIndex >= critterPool.Length)
                    {
                        currentCritterIndex = 0;
                    }

                }

                foreach (Critter eachCritter in critterPool)
                {
                    //loop contents

                    eachCritter.Input();

                }
            }
            else
            {
                startButton.Input();
            }
            

            
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here

            spriteBatch.Begin();

            foreach (Critter eachCritter in critterPool)
            {
                //loop contents

                eachCritter.Draw(spriteBatch);

            }
            ourScore.Draw(spriteBatch);

            gameTimer.Draw(spriteBatch);

            startButton.Draw(spriteBatch);

            spriteBatch.End();

            base.Draw(gameTime);
        }

        void StartGame()
        {
            playing = true;

            //reset score
            ourScore.resetScore();

            // Reset timer
            gameTimer.SetTimer(GAME_LENGTH);
            gameTimer.StartTimer();
        }

        void EndGame()
        {
            playing = false;

            //TODO: show the button
            startButton.Show();

            

            // despawn each critter
            foreach (Critter eachCritter in critterPool)
            {
                eachCritter.Despawn();
            }

            
            
            
        }
    }
}
